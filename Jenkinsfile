import jenkins.model.*

def Environment_Name = "stg-alln-01"
def containerName = "stg-alln-01-incubator"
def cluster_name = "stg-alln-01"

def cred = "gitlab"

def isTest() {
    return true
}


// ---------- pipeline resume

/* All functions are for pipelie resumption
 * Basically we save a json file named pipeline.${env.JOB_NAME}.json, in Openstacks swift/objectstore
 *
 * example:
 *  {
 *    "buildId": "100",
 *    "buildCompleted: false,
 *
 *    "currentStage": "Camel",
 *    "currentStore": { "key1": "val1" },
 *    "currentStageCompleted: [
 *      "Apple",
 *      "Banana"
 *    ],
 *
 *    "previousStage": "Camel",
 *    "previousStore": { "key1": "val1", "key2": "val2" },
 *    "previousStageCompleted: [
 *      "Apple",
 *      "Banana"
 *    ],
 *
 *    "buildResume": true,
 *    "buildResumeCount": 0
 *  }
 *
 **/


def uploadPipelineState() {
    def action = "put"
    return transferPipelineState(action)
}

def downloadPipelineState() {
    def action = "get"
    return transferPipelineState(action)
}

def transferPipelineState(action) {
    return true
    // just defaulting
    def swiftAction = action as String
    def stateFile = "pipeline.${env.JOB_NAME}.json"
    def containerName = "stg-alln-01-incubator"
    def osContainerName = readFile('storageFile').trim()
    def objStoreToken = readFile('tokenFile').trim()
    def retval = false

    if (action == "put") {
        if ((fileExists(stateFile)) == false) {
            input(id: 'pipelineStateFileUpload',
                message: "${stateFile} does not exist for uploading. Ignore and continue?",
                ok: 'Apply')
            return true
        }
    }
    if (action == "get") {
        if ((fileExists(stateFile)) == true) {
            input(id: 'pipelineStateFileDownload',
                message: "${stateFile} will be overwritten by objectstore version. Do you want to continue?",
                ok: 'Apply')
            return true
        }
    }
    if (osContainerName.contains(containerName)) {
        def containerFiles = readFile('containerFile').trim()
        if (containerFiles.contains(stateFile)) {
            def osCredentialsId = "openstack-${Env_Type}"
            withCredentials([usernamePassword(
                credentialsId: osCredentialsId,
                usernameVariable: 'OSUSERNAME',
                passwordVariable: 'OSPASSWORD')]) {
                    sh """
                        python3 ${WORKSPACE}/pythonUtil/objectstorage/objstorage_cli.py \
                            "${Project_Name}" -dc "${Data_Center}" \
                            -u "${OSUSERNAME}" -p "${OSPASSWORD}" > tokenFile
                    """
                    objStoreToken = readFile('tokenFile').trim()
                    sh """
                        python3 ${WORKSPACE}/pythonUtil/objectstorage/objstorage_cli.py \
                            "${Project_Name}" -dc "${Data_Center}" \
                            -a "${swiftAction}" -c "${containerName}" \
                            -o "${stateFile}" -t "${objStoreToken}"
                    """
                    retval = true
            }
        }
    }
    return retval
}

def getPipelineState() {
    def state = null
    def stateFile = "pipeline.${env.JOB_NAME}.json"
    try {
        if ((fileExists(stateFile))) {
            ps = readJSON file: stateFile
            state = ps
        }
        else {
            error("pipeline state file ${stateFile} not found")
        }
    }
    catch(err) {
        println("Error: ${err}")
        currentBuild.result = "FAILURE"
        messageContent = "Message: Cannot get pipeline state from json."
    }
    return state
}

def setPipelineState(pipelineState) {
    def stateFile = "pipeline.${env.JOB_NAME}.json"
    try {
        ps = writeJSON file: stateFile, json: pipelineState
    }
    catch (err) {
        println("Error: ${err}")
        currentBuild.result = "FAILURE"
        messageContent = " Message: Cannot set pipeline state."
    }
}

def updatePipelineState(newStage, completed = true) {
    def pipelineState = getPipelineState()
    def stateFile = "pipeline.${env.JOB_NAME}.json"
    def oldStage = pipelineState.currentStage

    if (oldStage == newStage) {
        // then its basically running stage again, so we should not mark complete.
        return pipelineState
    }

    if (completed) {
        if (pipelineState.currentStageCompleted.contains(oldStage) == false) {
            // ignore startBuild's init stage
            if (oldStage != 'startBuild@init') {
                pipelineState.currentStageCompleted << oldStage
            }
        }
    }

    pipelineState.currentStage = newStage

    try {
        ps = writeJSON file: stateFile, json: pipelineState
    }
    catch (err) {
        println("================ Error: ${err}")
        currentBuild.result = 'FAILURE'
        messageContent = "Message: Cannot save pipeline state in ${stateFile}."
    }
    return pipelineState
}

// pipeline usable functions
// start of pipeline
def startBuild(resume = true) {
    def newPipelineState = [
        buildId: "${env.BUILD_ID}",
        buildCompleted: false,
        buildResume: false,
        buildResumeCount: 0,
        currentStage: 'startBuild@init',
        currentStore: [:],
        currentStageCompleted: [],
        previousStage: "",
        previousStore: [:],
        previousStageCompleted: []
    ]

    if (downloadPipelineState()) {
        def oldPipelineState = getPipelineState()
        if (oldPipelineState.buildCompleted == false) {
            if (resume) {
                newPipelineState.buildResume = resume
                newPipelineState.previousStore = oldPipelineState.currentStore
                newPipelineState.previousStage = oldPipelineState.currentStage
                newPipelineState.previousStageCompleted = oldPipelineState.currentStageCompleted
                newPipelineState.buildResumeCount = oldPipelineState.buildResumeCount + 1
            }
        }
    }

    setPipelineState(newPipelineState)
    return newPipelineState
}

// end of pipeline
def buildComplete() {
    def pipelineState = getPipelineState()
    pipelineState.buildResume = false
    pipelineState.buildCompleted = true
    pipelineState.currentStageCompleted << pipelineState.currentStage
    pipelineState.currentStage = ''
    setPipelineState(pipelineState)
    uploadPipelineState()
    return pipelineState
}

// end of pipeline incomplete
def buildIncomplete() {
    def pipelineState = getPipelineState()
    pipelineState.buildCompleted = false
    setPipelineState(pipelineState)
    uploadPipelineState()
    return pipelineState
}

// start of a stage
def startStage(stageName) {
    return updatePipelineState(stageName, true)
}

// set kv in current store
def setCurrentStore(key, value) {
    def pipelineState = getPipelineState()
    pipelineState.currentStore[key as String] = value as String
    setPipelineState(pipelineState)
    return pipelineState
}

// get kv in current store
def getCurrentStore(key) {
    def pipelineState = getPipelineState()
    def value = pipelineState.currentStore[key as String]
    return value
}

// get kv in old/previous build's store
def getPreviousStore(key) {
    def pipelineState = getPipelineState()
    def value = pipelineState.previousStore[key as String]
    return value
}

// copy previous key's value to current key.
def syncStore(key) {
    def pipelineState = getPipelineState()
    pipelineState.currentStore[key as String] = pipelineState.previousStore[key as String] ?: null
    setPipelineState(pipelineState)
    return pipelineState.currentStore[key as String]
}

// when resuming, should I skip or run? And if run, then start.
def isStageRunnable(stageName) {
    def pipelineState = getPipelineState()
    def runnable = (pipelineState.currentStageCompleted.contains(stageName)) ? false : true

    if (pipelineState.buildResume && runnable) {
        runnable = (pipelineState.previousStageCompleted.contains(stageName)) ? false : true
    }

    if (runnable) {
        startStage(stageName)
    }
    return runnable
}

// when running, am I being resumed? or fresh Prince of Bel-Air
def isStageResume(stageName) {
    def pipelineState = getPipelineState()
    def doExecution = false
    if (pipelineState.buildResume) {
        doExecution = (pipelineState.previousStage == stageName) ? true : false
    }
    return doExecution
}



// ---------- pipeline resume



node("master") {

    timeout(time: 120, unit: 'MINUTES') {

        try {

            stage("scm checkout") {

                dir("stuff"){
                    git poll: false, changelog: false, url: "git@gitlab.com:gautsing/project-lol-1.git", credentialsId: "gitlab", branch: "main"
                    sh """
                        [[ -e "pipeline.${env.JOB_NAME}.json" ]] && cp -v "pipeline.${env.JOB_NAME}.json" ../ || : no file
                        [[ -e "pipeline.${env.JOB_NAME}.json" ]] && { cat "pipeline.${env.JOB_NAME}.json" | jq; } || : no file


                        [[ -e "clouds.yaml" ]] && cat "clouds.yaml" || : no file
                        [[ -e "clouds.yaml" ]] && { cp -v "clouds.yaml" ../; } || : no file
                    """
                }
            }

            stage("init") {
                // initialize pipeline
                startBuild(false)
                echo "init ${getPipelineState()}"
            }


            stage("A") {
                def stageName = "A"
                if (isStageRunnable(stageName)) {
                    // def cmd = 'oc get etcd cluster'
                    checkOcStability('oc get etcd cluster')

                    echo "Running ${stageName} ${getPipelineState()}"
                    if (isStageResume(stageName)) {
                        echo "Running (resume) ${stageName} ${getPipelineState()}"
                    }
                }
                else {
                    echo "Skipping ${stageName}"
                }
            }


            stage("C") {
                def stageName = "C"
                if (isStageRunnable(stageName)) {
                    echo "Running ${stageName} ${getPipelineState()}"
                    if (isStageResume(stageName)) {
                        echo "Running (resume) ${stageName} ${getPipelineState()}"
                    }
                    sh 'exit 1'
                }
                else {
                    echo "Skipping ${stageName}"
                }
            }

            stage("end") {
                buildComplete()
                echo "final ${getPipelineState()}"
            }

        }
        catch (err) {
            println("================ Error: ${err}")
            currentBuild.result = "FAILURE"
            messageContent = " Message: The Job failed.  Please investigate the root cause and execute again."
        }
        finally {
            stage("pipelineState uploader") {
                println("================== [start] pipeline.${env.JOB_NAME}.json")
                println(readFile("pipeline.${env.JOB_NAME}.json"))
                println("================== [ end ] pipeline.${env.JOB_NAME}.json")
                if (currentBuild.result == "FAILURE") {
                    buildIncomplete()
                }
                else {
                    buildComplete()
                }
            }

        }


    }


}




def checkOcStability(cmd) {
    def minConsecutiveSuccessCount = 10
    def totalCount = 0
    def consecutiveSuccessCount = 0

    println "[checkOcStability] checking stability for cmd: '" + cmd + "' for " +  minConsecutiveSuccessCount + " consecutive successful runs"

    while (consecutiveSuccessCount < minConsecutiveSuccessCount) {
        totalCount = totalCount + 1
        try {
            sh """
                ssh -q -o StrictHostKeyChecking=no root@stg-alln-01-controller.cisco.com "${cmd}"
            """
            consecutiveSuccessCount = consecutiveSuccessCount + 1
            println "[checkOcStability] total: " + totalCount + " consecutive_success: " + consecutiveSuccessCount
        }
        catch(err) {
            consecutiveSuccessCount = 0
            println "[checkOcStability] total: " + totalCount + " consecutive_success: " + consecutiveSuccessCount + " error: " + err
            sleep(30)
        }
    }

    println "[checkOcStability] stablity achieved. total: " + totalCount + " consecutive_success: " + consecutiveSuccessCount
}
