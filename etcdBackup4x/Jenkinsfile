import groovy.json.*
import groovy.json.JsonOutput;
import java.io.File.*;
import java.text.*;

def messageContent = "Message:"
def env_name = "${Environment_Name}"
def pythonUtilRepo = "https://eps-automation.gen@gitscm.cisco.com/scm/eps-kube/cae-python-utilities.git"

def backupStatusCheck = 1
def backupStatusJson = ""
def destDC = "rcdn"
def destProject = "CAE-NONPROD-"+destDC.toUpperCase()
def node_name = ""

def minBackupSizeBytes = 20000000

node
{
    ansiColor('xterm') {
    timeout(time: 1, unit: 'HOURS')
    {
        try
        {
            stage('Git Pull')
            {
                dir('pythonUtil') {
                    git pool: false, changelog: false, url: pythonUtilRepo, credentialsId: "git", branch: "master"
                }
            }
            def osCredentialsId = "openstack-${Env_Type}"

            stage('Get Controller Key')
            {
                withCredentials([usernamePassword(credentialsId: osCredentialsId, usernameVariable: 'OSUSERNAME', passwordVariable: 'OSPASSWORD')])
                {
                    sh """
                        echo "${Environment_Name} - Generate the new token file"
                        python3 pythonUtil/objectstorage/objstorage_cli.py "${Project_Name}" -dc "${Data_Center}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" > tokenFile
                    """

                    def OSTOKEN = readFile('tokenFile').trim()

                    def containerName="${Environment_Name}-incubator"

                    sh """
                        python3 pythonUtil/objectstorage/objstorage_cli.py "${Project_Name}" -a get -c "${containerName}" -dc "${Data_Center}" -o id_rsa -t "${OSTOKEN}"
                        chmod 600 ${WORKSPACE}/id_rsa
                    """
                }
            }

            stage ('Check rclone Installation')
            {
                try {
                    sh "curl https://rclone.org/install.sh | sudo bash"
                }
                catch (e) {
                    println("Rclone is already installed")
                }
            }

            stage ('Generate and zip ETCD backup')
            {
                //node_name = sh(returnStdout: true, script: "ssh -i ${WORKSPACE}/id_rsa  -o StrictHostKeyChecking=no -q root@${Environment_Name}-controller.cisco.com oc get nodes --no-headers | grep master | grep -v NotReady | awk 'NR==1{print \$1 | \"sort\" }' ").trim()
				//def node_name = sh(returnStdout: true, script: "ssh -i ${WORKSPACE}/id_rsa  -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com \"oc get nodes -o=custom-columns=NODE:.metadata.name --no-headers | grep master-0 \"").trim()

                sh"""
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "mkdir -p /root/etcd-backup"
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "rm -f /root/etcd-backup/*"
                scp -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no ${WORKSPACE}/id_rsa root@${Environment_Name}-controller.cisco.com:/root/
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "chmod 400 id_rsa"
                """
                def node_name_list = sh(returnStdout: true, script: "ssh -i ${WORKSPACE}/id_rsa  -o StrictHostKeyChecking=no -q root@${Environment_Name}-controller.cisco.com oc get nodes --no-headers | grep master | grep -v NotReady | awk '{print \$1 | \"sort\" }' ").trim().split("\n")
                def backup_success = 0
                for (int i=0; i<node_name_list.size() && backup_success == 0; i++)
                {
                    node_name =  node_name_list[i].trim()
                    print node_name
                    try
                    {
                        sh """
                            ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "ssh -i id_rsa -o StrictHostKeyChecking=no core@${node_name} sudo rm -f /home/core/assets/backup/*"
                            ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "ssh -i id_rsa -o StrictHostKeyChecking=no core@${node_name} sudo /usr/local/bin/cluster-backup.sh /home/core/assets/backup" | tee -a ${node_name}.cluster-backup.log
                            ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "ssh -i id_rsa -o StrictHostKeyChecking=no core@${node_name} sudo chown core /home/core/assets/backup/*"
                            ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "scp -i id_rsa -o StrictHostKeyChecking=no core@${node_name}:/home/core/assets/backup/* /root/etcd-backup/"

                            if [ \$(grep -c 'snapshot db and kube resources are successfully saved' ${node_name}.cluster-backup.log) == 1 ]; then
                                if [ \$(du -sm /root/etcd-backup/ | cut -f1) -gt 20" ]; then
                                    echo 'backup is successfull, and backup size greater than 20 Megabytes'
                                else
                                    echo 'backup size is too small, not a good backup'
                                    exit 1
                                fi
                            else
                                echo 'cluster-backup script log does not say it was successful'
                                exit 1
                            fi
                        """
                        backup_success = 1
                    }
                  	catch (err)
                    {
                        println("Backup failed from node " + node_name)
                        backup_success = 0
                    }
                }
                sh"""
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "tar -czvf etcd-backup.tgz etcd-backup"
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "mv etcd-backup.tgz etcd-backup-\$(date +%Y-%m-%d-%H:%M:%S).tgz"
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "split -b 1500m etcd-backup-*.tgz etcd-backup-\$(date +%Y-%m-%d-%H:%M:%S).tgz."
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "rm -f etcd-backup-*.tgz"
                scp -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com:/root/etcd-backup-* .
                ssh -i ${WORKSPACE}/id_rsa -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com "rm -f etcd-backup-*"
                """
            }

            long size=0L
            stage('Get backup file size')
            {
               sh """
               find . -name '*etcd-backup*' -exec ls -l {} \\; | awk '{print \$5}' > file_size
               """
               def file_size_string = readFile 'file_size'
               def file_size_list = file_size_string.tokenize('\n')
               for (def i : file_size_list)
               {
                    size  = size + i.toInteger()
               }
               println ("SIZE")
               println (size)

               if (size < minBackupSizeBytes) {
                   error('size ' + size + ' is lesser than min etcd backup size 20MB ' + minBackupSizeBytes)
               }
            }

            stage('Upload backup and related metadata to Objectstore')
            {
                uploadBackupFiles(size)
                try
                {
                    //def node_name = sh(returnStdout: true, script: "ssh -i ${WORKSPACE}/id_rsa  -o StrictHostKeyChecking=no root@${Environment_Name}-controller.cisco.com \"oc get nodes -o=custom-columns=NODE:.metadata.name --no-headers | grep master-0 \"").trim()

                    sub_node_name = node_name.substring(0, node_name.length() - 1)
                    if (sub_node_name.contains(env_name))
                        withCredentials([usernamePassword(credentialsId: osCredentialsId, usernameVariable: 'OSUSERNAME', passwordVariable: 'OSPASSWORD')])
                        {
                            sh """
                                echo ${sub_node_name} > recovery_host_file
                                source venv/bin/activate
                                python3 pythonUtil/objectstore_backup/rotate_backup.py "${Project_Name}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" -c "${Environment_Name}-incubator" -dc "${Data_Center}" -a put -o "recovery_host_file"
                            """
                        }
                }
                catch (err)
                {
                    println("Failed to upload recovery host file")
                }
            }

            stage('Upload to AWS S3')
            {
                def jobsRepo = "https://eps-automation.gen@gitscm.cisco.com/scm/eps-kube/cae-jenkins-pipeline.git"
                dir('cae-jenkins-pipeline')
                {
                    git branch: 'master', changelog: false, credentialsId: 'git', poll: false, url: jobsRepo
                }
                def r=readFile("${WORKSPACE}/lsoutput")
                def etcdbackupfilenamelist = r.split("\n")
                def etcdbackupfilecount = etcdbackupfilenamelist.size()
                print("etcd backup file count : " + etcdbackupfilecount)
                withAWS (region:'us-east-1',credentials:'awsbackupcreds') {
                    sh "rclone --config=\"cae-jenkins-pipeline/etcdBackup/rclone.conf\" mkdir awscae:${Environment_Name}-backups"
                    for (etcdbackupfile in etcdbackupfilenamelist)
                    {
                        ("Uploading to AWS etcd backup file " + etcdbackupfile )
                        def etcdbackupfileTrimmed = etcdbackupfile.trim()
                        sh "rclone --config=\"cae-jenkins-pipeline/etcdBackup/rclone.conf\" copy ${WORKSPACE}/$etcdbackupfileTrimmed awscae:${Environment_Name}-backups"
                    }
                    /*if (etcdbackupfilecount == 1)
                    {
                        sh "rclone --config=\"cae-jenkins-pipeline/etcdBackup/rclone.conf\" copy ${WORKSPACE}/etcd-backup-* awscae:${Environment_Name}-backups"
                    }
                    else
                    {
                        sh "rclone --config=\"cae-jenkins-pipeline/etcdBackup/rclone.conf\" copy --include ${WORKSPACE}/etcd-backup-* awscae:${Environment_Name}-backups"
                    }*/
                }
            }
        }
        catch (err)
        {
            println("================ Error: ${err}")
            currentBuild.result = "FAILURE"
            messageContent = " Message: The Job failed.  Please investigate the root cause and execute again."
        }
        finally {

            stage ('Send Notification to Outlook')
            {
                def jobsRepo = "https://eps-automation.gen@gitscm.cisco.com/scm/eps-kube/cae-jenkins-pipeline.git"
                dir('cae-jenkins-pipeline')
                {
                    git branch: 'master', changelog: false, credentialsId: 'git', poll: false, url: jobsRepo
                }
                def remotefuctioncall = load "cae-jenkins-pipeline/common_libraries/SendToOutlook.groovy"
                remotefuctioncall.sendToOutlook(Environment_Name, "Job ${JOB_NAME} (Build #${BUILD_NUMBER}) status : [${currentBuild.currentResult}]", "${messageContent} <br /> For more detail: ${BUILD_URL} <br />", "no", "")
            }

            if (currentBuild.currentResult != "SUCCESS" || currentBuild.getPreviousBuild().getResult().toString() != "SUCCESS")
            {
                stage ('Send Notification to Webex Teams')
                {
                    def jobsRepo = "https://eps-automation.gen@gitscm.cisco.com/scm/eps-kube/cae-jenkins-pipeline.git"
                    dir('cae-jenkins-pipeline')
                    {
                        git branch: 'master', changelog: false, credentialsId: 'git', poll: false, url: jobsRepo
                    }
                    def remotefuctioncall = load "cae-jenkins-pipeline/common_libraries/SendToWebexTeams.groovy"
                    remotefuctioncall.sendToWebexTeams(Environment_Name, "Job '${JOB_NAME}' (Build #${BUILD_NUMBER}) status : [${currentBuild.currentResult}]. For more detail: ${BUILD_URL}", "environment_specific", "", "")
                }
            }

            stage ('Clean Workspace') {
                cleanWs()
            }
        }
    }
}
}

class BackupMetadata {
    //DecimalFormat df = new DecimalFormat("0.00");
    float df = 0.00f;
    float sizeKb = 1024.0f;
    float sizeMb = sizeKb * sizeKb;
    float sizeGb = sizeMb * sizeKb;
    float sizeTerra = sizeGb * sizeKb;
    float sizePeta = sizeTerra * sizeKb;
    //name of the etcd backup file
    String name
    //size of the backup file in bytes
    long size
    String dateOfBackup
    String strDateTimeOfBackup

    String hash
    String revision
    String totalKey
    //clusterDetails for which backup is create, should be in format <lifecycle-dcname>
    String clusterDetails
    //contains all backup split file name(s) uploaded as part of backup,
    //if single file then that will also be available in this set only
    Set<String> multipartFileNames = new HashSet<String>()

    public String getHumanReadableBackupSize() {
        if(size < sizeMb)
            return (size / sizeKb)+ " Kb";
        else if(size < sizeGb)
            return (size / sizeMb) + " Mb";
        else if(size < sizeTerra)
            return (size / sizeGb) + " Gb";
        else if(size < sizePeta)
            return (size / sizeTerra) + " Tb";
        else
            return (size / sizePeta) + " Pb";
        return ""
    }

    public AppendMultipartFileName(String fileName){
        multipartFileNames.add(fileName)
    }

    public getBackupMetaMap(){
        def metaDataMap = [
            name: name,
            size: getHumanReadableBackupSize(),
            dateOfBackup: dateOfBackup,
            clusterDetails: clusterDetails,
            splitFiles:multipartFileNames,
            hashStatusCheck: hash,
            revisionStatusCheck: revision,
            totalKeyStatusCheck: totalKey
        ]
        return metaDataMap
    }
    public getMetaFileName(){
        return 'metadata-etcd-backup-'+strDateTimeOfBackup+'.json'
    }
}

def getBackupMetadataObject(partFileName,fileSize,projectName) {
    def fileName=""
    def dateTimeOfBackup=""
    dateTimeOfBackup=partFileName.split("-backup-")[1]
    dateTimeOfBackup=dateTimeOfBackup.split("\\.")[0]
    fileNamePrefix="etcd-backup-"
    fileNameWildCard=fileNamePrefix+dateTimeOfBackup+".tgz.*"

    def parser = new JsonSlurper()

    BackupMetadata backupMeta=new BackupMetadata(name:fileNameWildCard,size:fileSize,strDateTimeOfBackup:dateTimeOfBackup,dateOfBackup:dateTimeOfBackup,clusterDetails:projectName)
    return backupMeta
}

def generateBackupFileMetadata(backupMeta) {
    def metaDataMap = backupMeta.getBackupMetaMap()
    def jsonMeta = JsonOutput.toJson(metaDataMap)
    prettyJsonMeta = JsonOutput.prettyPrint(jsonMeta)
    metaDataFileName=backupMeta.getMetaFileName()
    writeFile(file:metaDataFileName, text: prettyJsonMeta)
    return metaDataFileName
}

def uploadBackupFiles(size){

    def osCredentialsId = "openstack-${Env_Type}"
    withCredentials([usernamePassword(credentialsId: osCredentialsId, usernameVariable: 'OSUSERNAME', passwordVariable: 'OSPASSWORD')])
    {
        sh """
            virtualenv venv -p python3
            source venv/bin/activate
            pip install -r ${WORKSPACE}/pythonUtil/objectstore_backup/requirements.txt
            pip install -r ${WORKSPACE}/pythonUtil/objectstorage/requirements.txt
            ls etcd-backup-* > ${WORKSPACE}/lsoutput
        """
        def r=readFile("${WORKSPACE}/lsoutput")
        def i =0
        def var=r.split("\n")

        if (var.size()>0) {
            def backupMetadataObject=getBackupMetadataObject(var[0],size, "${Environment_Name}")
            r=readFile("${WORKSPACE}/lsoutput")
            i =0
            var=r.split("\n")
            def a=""

            if("${Data_Center}" == "rcdn")
            {
                destDC ="alln"
            }
            else if("${Data_Center}" == "alln")
            {
                destDC ="rtp"
            }
            else if("${Data_Center}" == "rtp")
            {
                destDC ="rcdn"
            }

            def envtype="${Env_Type}"
            if(envtype == "prod")
            {
                destEnv="CAE-PROD-"
            }
            else if(envtype == "non-prod")
            {
                destEnv="CAE-NONPROD-"
            }
            else
            {
                destEnv = "CAE-IDEV-P-"
            }

            destProject = destEnv.toUpperCase()+destDC.toUpperCase()

            def not_uploaded = 1
			println("No. of etcd backup split parts :" + var.size())
            for (i = 0; i < var.size(); i++)
            {
                a=var[i]
                backupMetadataObject.AppendMultipartFileName(a)
                sh """
                    source venv/bin/activate
                    """
                 def counter = 0
                 def max_retries = 30
                 not_uploaded = 1
                 while(not_uploaded != 0 && counter < max_retries)
                 {
                    counter = counter + 1
                    try{
                        sh """
                        source venv/bin/activate
                        python3 pythonUtil/objectstore_backup/rotate_backup.py "${Project_Name}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" -c "${Environment_Name}-etcd-backup" -dc "${Data_Center}" -a put -o "$a"
                        python3 pythonUtil/objectstore_backup/rotate_backup.py "${destProject}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" -c "objectstore-backup-${Environment_Name}" -dc "${destDC}" -a put -o "$a"
                        """
                        not_uploaded = 0
                        //break
                    }
                    catch(err) {
                        sleep(5)
                        println("Error uploading to openstack. Trying Again")
                        if (counter == max_retries - 1)
                        {
                            currentBuild.result = "FAILURE"
                        }
                        println(max_retries - counter + " retries remaining")
                    }
                 }
            }
            def backupMetadataFile = generateBackupFileMetadata(backupMetadataObject)
            //Upload the backup metadata file to object store
            def not_uploaded_metadata = 1
            def counter_metadata = 0
            def max_retries = 30
            while(not_uploaded_metadata && counter_metadata < max_retries && !not_uploaded)
            {
                counter_metadata = counter_metadata + 1
                try {
                    sh """
                    source venv/bin/activate
                    python3 pythonUtil/objectstore_backup/rotate_backup.py "${Project_Name}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" -c "${Environment_Name}-etcd-backup" -dc "${Data_Center}" -a put -o "$backupMetadataFile"
                    """
                    not_uploaded_metadata = 0
                    break
                }
                catch (err) {
                    sleep(5)
                    println("Error uploading to openstack. Trying Again")
                    if (counter_metadata == max_retries - 1)
                    {
                        currentBuild.result = "FAILURE"
                    }
                    println(max_retries - counter_metadata + " retries remaining")
                }
            }
            sh """
                source venv/bin/activate
                python3 pythonUtil/objectstore_backup/rotate_backup.py  "${Project_Name}" -dc "${Data_Center}" -u "${OSUSERNAME}" -p "${OSPASSWORD}" -c "${Environment_Name}-etcd-backup" -a delete -d 7
            """
        }else{
            error "Nothing to upload as backup file(s) list is empty!!!!"
        }
    }
}
